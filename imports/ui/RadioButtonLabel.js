import React from 'react';

export default class RadioButtonLabel extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            checked: true
        };

        this.radioButtonHandler = this.radioButtonHandler.bind(this);
    }

    radioButtonHandler(event){
        event.preventDefault();

        this.setState({
            checked: event.target.checked
        });

        var value = event.target.value;

        console.log(value);
    }

    render() {

        return(
            <div>
                    <input type="radio"  onChange={this.radioButtonHandler} id="r1" name="rate" value="Fixed Rate"/> Fixed Rate
                    <input type="radio"  onChange={this.radioButtonHandler} id="r2" name="rate" value="Variable Rate"/> Variable Rate
                    <input type="radio"  onChange={this.radioButtonHandler}  id="r3" name="rate" value="Multi Rate"/> Multi Rate
            </div>
        )
    }
}