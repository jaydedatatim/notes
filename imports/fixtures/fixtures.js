export const notes = [
    {
        _id: '123abc',
        title: 'Test title',
        body: '',
        updatedAt:1486137505429,
        userId:'userId1'
    },
    {
        _id: 'noteId2',
        title: '',
        body: 'Some text here',
        updatedAt:1486137505429,
        userId:'userId2'
    }
];